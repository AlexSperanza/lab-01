import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class AbstractBankAccountTest {

    private AccountHolder accountHolder;
    private BankAccount bankAccount;

    protected final void setAccountHolder(final AccountHolder accountHolder) {
        this.accountHolder = accountHolder;
    }

    protected final void setBankAccount(final BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    protected final AccountHolder getAccountHolder() {
        return this.accountHolder;
    }

    protected final BankAccount getBankAccount() {
        return this.bankAccount;
    }

    protected final void assertDeposit(final double amount, final double expectedBalance) {
        this.getBankAccount().deposit(accountHolder.getId(), amount);
        assertEquals(expectedBalance, bankAccount.getBalance());
    }

    protected final void assertWithdraw(final double initialDepositAmount, final double amount, final double expectedBalance) {
        bankAccount.deposit(accountHolder.getId(), initialDepositAmount);
        bankAccount.withdraw(accountHolder.getId(), amount);
        assertEquals(expectedBalance, bankAccount.getBalance());
    }

    protected final void assertWrongDeposit(final double initialDepositAmount, final double expectedBalance, final int wrongAccountId) {
        final double stubDepositAmount = 50;
        bankAccount.deposit(accountHolder.getId(), initialDepositAmount);
        bankAccount.deposit(wrongAccountId, stubDepositAmount);
        assertEquals(expectedBalance, bankAccount.getBalance());
    }

    protected final void assertWrongWithdraw(final double initialDepositAmount, final double expectedBalance, final int wrongAccountId) {
        final double stubWithdrawAmount = 70;
        this.getBankAccount().deposit(accountHolder.getId(), initialDepositAmount);
        this.getBankAccount().withdraw(wrongAccountId, stubWithdrawAmount);
        assertEquals(expectedBalance, bankAccount.getBalance());
    }

    @Test
    void testInitialBalance() {
        assertEquals(0, bankAccount.getBalance());
    }
}
