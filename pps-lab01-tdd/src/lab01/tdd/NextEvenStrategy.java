package lab01.tdd;

public abstract class NextEvenStrategy extends MultipleOfStrategy implements SelectStrategy {

    private static final int BASE_NUMBER = 2;

    public NextEvenStrategy() {
        super(BASE_NUMBER);
    }
}
