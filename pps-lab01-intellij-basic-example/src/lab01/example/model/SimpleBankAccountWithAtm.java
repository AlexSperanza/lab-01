package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccount {

    private static final int OPERATION_FEE = 1;

    public SimpleBankAccountWithAtm(AccountHolder accountHolder, int i) {
        super(accountHolder, i);
    }

    @Override
    public void deposit(final int usrID, final double amount) {
        super.deposit(usrID, amount - OPERATION_FEE);
    }

    @Override
    public void withdraw(final int usrID, final double amount) {
        super.withdraw(usrID, amount + OPERATION_FEE);
    }
}
