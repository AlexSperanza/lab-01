package lab01.tdd;

public abstract class EqualsStrategy implements SelectStrategy {

    private final int comparisonNumber;

    public EqualsStrategy(final int comparisonNumber) {
        this.comparisonNumber = comparisonNumber;
    }

    @Override
    public boolean apply(final int element) {
        return element == this.comparisonNumber;
    }
}
