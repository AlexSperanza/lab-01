import lab01.tdd.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private CircularList list;
    private StrategyAbstractFactory strategyFactory;

    @BeforeEach
    void beforeEach() {
        list = new SimpleCircularList();
        strategyFactory = ConcreteStrategyFactory.getInstance();
    }

    @Test
    void testIsEmpty() {
        assertTrue(list.isEmpty());
    }

    @Test
    void testAddToList() {
        list.add(0);
        assertFalse(list.isEmpty());
    }

    @Test
    void testSize() {
        final int expectedValue = 1;
        list.add(0);
        assertEquals(expectedValue, list.size());
    }

    @Test
    void testNext() {
        final int expectedValue = 0;
        list.add(0);
        assertEquals(Optional.of(expectedValue), list.next());
    }

    @Test
    void testPrevious() {
        final int expectedValue = 1;
        list.add(0);
        list.add(1);
        list.next();
        assertEquals(Optional.of(expectedValue), list.previous());
    }

    @Test
    void testPreviousAfterAdd() {
        final int expectedValue = 1;
        list.add(0);
        list.add(1);
        assertEquals(Optional.of(expectedValue), list.previous());
    }

    @Test
    void testReset() {
        final int expectedValue = 0;
        list.add(0);
        list.add(1);
        list.next();
        list.reset();
        assertEquals(Optional.of(expectedValue), list.next());
    }

    @Test
    void testNextEvenStrategy() {
        final int expectedValue = 2;
        list.add(1);
        list.add(2);
        list.add(3);

        assertOptionalIntFailIfNotPresent(expectedValue, list.next(strategyFactory.getEvenStrategy()));
    }

    @Test
    void testNextStrategyCircularity() {
        final SelectStrategy nextEven = strategyFactory.getEvenStrategy();
        final int expectedValue = 2;
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        list.next(nextEven);
        list.next(nextEven);

        assertOptionalIntFailIfNotPresent(expectedValue, list.next(nextEven));
    }

    @Test
    void testNextMultipleOfStrategy() {
        final int baseNumber = 4;
        final int expectedNumber = 8;
        final SelectStrategy multipleOfStrategy = strategyFactory.getMultipleOfStrategy(baseNumber);
        list.add(3);
        list.add(8);
        list.add(9);
        list.add(13);

        assertOptionalIntFailIfNotPresent(expectedNumber, list.next(multipleOfStrategy));
    }

    @Test
    void testNextEqualsStrategy() {
        final int comparisonNumber = 2;
        final int expectedNumber = 2;
        final SelectStrategy equalStrategy = strategyFactory.getEqualsStrategy(comparisonNumber);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);

        assertOptionalIntFailIfNotPresent(expectedNumber, list.next(equalStrategy));
    }

    @Test
    void testNextStrategyOnEmptyList() {
        final SelectStrategy strategy = strategyFactory.getEvenStrategy();
        final Optional<Integer> expected = Optional.empty();
        assertEquals(expected, list.next(strategy));
    }

    private void assertOptionalIntFailIfNotPresent(final int expected, final Optional<Integer> optionalValue) {
        if (optionalValue != null && optionalValue.isPresent()) {
            assertEquals(expected, optionalValue.get());
        } else {
            fail("Value not present in the Optional object.");
        }
    }
}
