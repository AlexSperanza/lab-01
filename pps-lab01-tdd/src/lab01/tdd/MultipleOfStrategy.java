package lab01.tdd;

public abstract class MultipleOfStrategy extends EqualsStrategy implements SelectStrategy {

    private final static int REMAINDER_OF_MULTIPLE = 0;
    private final int baseNumber;

    public MultipleOfStrategy(final int baseNumber) {
        super(REMAINDER_OF_MULTIPLE);
        this.baseNumber = baseNumber;
    }

    @Override
    public boolean apply(final int element) {
        final int remainder = element % this.baseNumber;
        return super.apply(remainder);
    }
}
