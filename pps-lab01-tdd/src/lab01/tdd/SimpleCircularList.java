package lab01.tdd;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SimpleCircularList implements CircularList {

    private final static int ITERATOR_BASE_VALUE = -2;

    private final List<Integer> list;
    private int listIterator;

    public SimpleCircularList() {
        this.list = new ArrayList<>();
        this.listIterator = ITERATOR_BASE_VALUE;
    }

    @Override
    public void add(int element) {
        this.list.add(element);
    }

    @Override
    public int size() {
        return this.list.size();
    }

    @Override
    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        final int simpleStep = 1;
        final int nextInitialValue = -1;
        return Optional.ofNullable(this.iterate(nextInitialValue, simpleStep));
    }

    @Override
    public Optional<Integer> previous() {
        final int simpleStep = -1;
        final int previousInitialValue = 0;
        return Optional.ofNullable(this.iterate(previousInitialValue, simpleStep));
    }

    @Override
    public void reset() {
        this.listIterator = ITERATOR_BASE_VALUE;
    }

    @Override
    public Optional<Integer> next(final SelectStrategy strategy) {
        final int oldListIterator = this.listIterator;
        do {
            Optional<Integer> value = this.next();
            if (value.isPresent() && strategy.apply(value.get())) {
                return value;
            }
        } while (this.listIterator != oldListIterator);
        return Optional.empty();
    }

    private Integer iterate(final int initialValue, final int step) {
        if (this.list.size() <= 0) {
            return null;
        } else {
            if (this.listIterator == ITERATOR_BASE_VALUE) {
                this.listIterator = initialValue;
            }
            this.listIterator = (this.listIterator + step + this.list.size()) % this.list.size();
            return this.list.get(this.listIterator);
        }
    }
}
