package lab01.example.model;

public abstract class AbstractSimpleBankAccount implements BankAccount {

    private double balance;
    private AccountHolder accountHolder;

    AbstractSimpleBankAccount(final AccountHolder accountHolder, final double balance) {
        this.accountHolder = accountHolder;
        this.balance = balance;
    }

    protected final void setBalance(final double balance) {
        this.balance = balance;
    }

    @Override
    public double getBalance() {
        return this.balance;
    }

    protected final void setHolder(final AccountHolder accountHolder) {
        this.accountHolder = accountHolder;
    }

    @Override
    public AccountHolder getHolder() {
        return this.accountHolder;
    }
}
