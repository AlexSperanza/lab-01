import lab01.example.model.AccountHolder;
import lab01.example.model.SimpleBankAccount;

import org.junit.jupiter.api.*;

/**
 * The test suite for testing the SimpleBankAccount implementation
 */
class SimpleBankAccountTest extends AbstractBankAccountTest {

    @BeforeEach
    void beforeEach(){
        this.setAccountHolder(new AccountHolder("Mario", "Rossi", 1));
        this.setBankAccount(new SimpleBankAccount(this.getAccountHolder(), 0));
    }

    @Test
    void testDeposit() {
        final double depositAmount = 100;
        final double expectedBalance = 100;
        this.assertDeposit(depositAmount, expectedBalance);
    }

    @Test
    void testWrongDeposit() {
        final double initialDepositAmount = 100;
        final double expectedBalance = 100;
        final int wrongId = 2;
        this.assertWrongDeposit(initialDepositAmount, expectedBalance, wrongId);
    }

    @Test
    void testWithdraw() {
        final double initialDepositAmount = 100;
        final double withdrawAmount = 70;
        final double expectedBalance = 30;
        this.assertWithdraw(initialDepositAmount, withdrawAmount, expectedBalance);
    }

    @Test
    void testWrongWithdraw() {
        final double initialDepositAmount = 100;
        final double expectedBalance = 100;
        final int wrongId = 2;
        this.assertWrongWithdraw(initialDepositAmount, expectedBalance, wrongId);
    }
}
