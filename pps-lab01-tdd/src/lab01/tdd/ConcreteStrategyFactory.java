package lab01.tdd;

import java.util.function.Function;

public class ConcreteStrategyFactory implements StrategyAbstractFactory {

    private final static ConcreteStrategyFactory instance = new ConcreteStrategyFactory();

    private ConcreteStrategyFactory() {}

    public static ConcreteStrategyFactory getInstance() {
        return instance;
    }

    @Override
    public SelectStrategy getEvenStrategy() {
        return new NextEvenStrategy() {};
    }

    @Override
    public SelectStrategy getMultipleOfStrategy(final int factorNumber) {
        return new MultipleOfStrategy(factorNumber) {};
    }

    @Override
    public SelectStrategy getEqualsStrategy(final int comparisonNumber) {
        return new EqualsStrategy(comparisonNumber) {};
    }

    @Override
    public SelectStrategy getCustomStrategy(final Function<Integer, Boolean> filter) {
        return filter::apply;
    }
}
