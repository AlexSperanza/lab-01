package lab01.tdd;

import java.util.function.Function;

/**
 * Represents a factory for creating SelectStrategies.
 */
public interface StrategyAbstractFactory {

    /**
     *  Returns a SelectStrategy which selects an even value.
     */
    SelectStrategy getEvenStrategy();

    /**
     * Returns a SelectStrategy which selects multiples of the provided value.
     */
    SelectStrategy getMultipleOfStrategy(final int factorNumber);

    /**
     * Returns a SelectStrategy which selects values equal to the provided one.
     */
    SelectStrategy getEqualsStrategy(final int comparisonNumber);

    /**
     * Returns a SelectStrategy which selects values based on a user-defined filter.
     */
    SelectStrategy getCustomStrategy(final Function<Integer, Boolean> filter);
}
