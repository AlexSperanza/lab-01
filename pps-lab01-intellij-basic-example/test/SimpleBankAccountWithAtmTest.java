import lab01.example.model.AccountHolder;
import lab01.example.model.SimpleBankAccountWithAtm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SimpleBankAccountWithAtmTest extends AbstractBankAccountTest {

    @BeforeEach
    void beforeEach() {
        this.setAccountHolder(new AccountHolder("Mario", "Rossi", 1));
        this.setBankAccount(new SimpleBankAccountWithAtm(this.getAccountHolder(), 0));
    }

    @Test
    void testDeposit() {
        final double depositAmount = 100;
        final double expectedBalance = 99;
        assertDeposit(depositAmount, expectedBalance);
    }

    @Test
    void testWrongDeposit() {
        final double initialDepositAmount = 100;
        final double expectedBalance = 99;
        final int wrongAccountId = 2;
        this.assertWrongDeposit(initialDepositAmount, expectedBalance, wrongAccountId);
    }

    @Test
    void testWithdraw() {
        final double initialDepositAmount = 100;
        final double amount = 70;
        final int expectedBalance = 28;
        assertWithdraw(initialDepositAmount, amount, expectedBalance);
    }

    @Test
    void testWrongWithdraw() {
        final double initialDepositAmount = 100;
        final double expectedBalance = 99;
        final int wrongAccountId = 2;
        this.assertWrongWithdraw(initialDepositAmount, expectedBalance, wrongAccountId);
    }
}
